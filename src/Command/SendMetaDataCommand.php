<?php

namespace CreareX\IVS\MetaDataHandler\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Symfony\Component\Console\Command\Command as SymfonyCommand;

class SendMetaDataCommand extends SymfonyCommand
{

    private $client;

    // public function __construct()
    public function __construct(HttpClientInterface $client = null)
    {
        parent::__construct();
        $this->client = ($client) ? 
            $client : 
            new \GuzzleHttp\Client();
    }

    public function configure()
    {
        $this->setName('sendMetaData')
            ->setDescription('Send metadata to ARN. ')
            ->setHelp('This Command sends IVS Event Metadata to a configurable endpoint')
            ->addArgument('arn', InputArgument::REQUIRED, 'The ARN of the ivs-channel that is to recieve an Event.')
            ->addArgument('metadata', InputArgument::REQUIRED, 'The metadata of the event in JSON Format');
            // ->addArgument('endpoint', InputArgument::REQUIRED, '(Optional) ');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {

        $params = array_combine($input->getOptions(), $input->getArguments());
        if (!($input->hasOption('arn') && $input->hasArgument('arn'))) {
            $params['arn'] = $this->getARNfromEnvFile();
        }

        if (!$input->hasOption('metadata') && $input->hasArgument('metadata')) {
            $params['metadata'] = $this->getDefaultJSONfromEnvFile();
        }

        if (!$input->hasOption('endpoint') && $input->hasArgument('endpoint')) {
            $params['endpoint'] = $this->getDefaultEndpointFromEnvFile();
        }

        $response = $this->sendMetadata($params);
        $this->output->writeln($response->getStatusCode());
    }

    protected function getJSONfromFilename($filename) : string {
        $filesystem = new Filesystem();
        if ($filesystem->exists($filename)) {
            return file_get_contents($filename);
        }
        throw new \Symfony\Component\Filesystem\Exception\IOException("File $filename not found. ");
    }

    protected function getJSON($string) : string {

        if ($arr = json_decode($string)) {
            return $string;
        }

        return $this->getJSONfromFilename($string);
    }

    protected function getARNfromEnvFile() : string {
        return getenv('ARN');
    }

    protected function getDefaultJSONfromEnvFile() : string {
        $jsonFilename = getenv('DEFAULT_JSON_FILE');
        return $this->getJSONfromFilename($jsonFilename);
    }

    protected function getDefaultEndpointFromEnvFile() : string {
        return getenv('DEFAULT_ENDPOINT');
    }

    protected function sendMetadata(array $params) : ResponseInterface {
        return $this->client->request(
            'POST',
            $params['endpoint'],
            [
                'json' => [
                    'arn' => $params['arn'],
                    'metadata' => $params['metadata']
                ]
            ]
        );
    }

}
