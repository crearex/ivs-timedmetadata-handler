#!/usr/bin/env php

<?php
require_once __DIR__ . '/../vendor/autoload.php';

use Symfony\Component\Console\Application;
use Symfony\Component\Dotenv\Dotenv;
use CreareX\IVS\MetaDataHandler\Command\SendMetaDataCommand;


$dotenv = new Dotenv();
$dotenv->load(__DIR__.'/.env');

$app = new Application('Console App', 'v1.0.0');
$app->add(new SendMetaDataCommand());
$app->run();

